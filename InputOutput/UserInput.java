package InputOutput;

import java.io.IOException;
import java.io.InputStreamReader;

public class UserInput {

	public static void main(String[] args) {
		InputStreamReader cin=null;
		try
		{
			cin=new InputStreamReader(System.in);
			System.out.println("enter characters,'q' to exit ");
			char c;
			do
			{
				c=(char)cin.read();
				System.out.println(c);
			}
			while(c!='q');
		}catch(IOException e)
		{
			e.getStackTrace();
		}

	}

}
